from django.urls import path
from .views import ListAndCreateTodosView, TodoDetail

urlpatterns = [
    path('todos/', ListAndCreateTodosView.as_view(), name='todos-list-create'),
    path('todos/<int:pk>', TodoDetail.as_view(), name='todo-detail')
]
