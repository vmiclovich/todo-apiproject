from rest_framework.response import Response
from rest_framework.views import status


def validate_request_data(func):
    def decorated(*args, **kwargs):
        item = args[0].request.data.get('item', '')

        if not item:
            return Response(data={
                "message": "An item is required"
            }, status=status.HTTP_400_BAD_REQUEST)
        return func(*args, **kwargs)
    return decorated