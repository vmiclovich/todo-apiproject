from django.test import TestCase
from django.urls import reverse
from rest_framework.test import APITestCase, APIClient
from rest_framework.views import status
import json

from .models import Todo
from .serializers import TodosSerializer


class BaseViewTest(APITestCase):
    client = APIClient()

    @staticmethod
    def create_todo(item=''):
        if item != '':
            Todo.objects.create(item=item)

    def setUp(self):
        # some test data
        self.create_todo('buy tomatoes')
        self.create_todo('pick kids up')


class GetAllTodosTest(BaseViewTest):

    def test_get_all_todos(self):
        response = self.client.get(reverse('todos-list-create', kwargs={'version': 'v1'}))
        self.assertEqual(response.data, TodosSerializer(Todo.objects.all(), many=True).data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class GetOneTodoItemTest(BaseViewTest):

    def test_fetch_item(self):
        valid_id = 1
        response = self.client.get(reverse('todo-detail', kwargs={'version': 'v1', 'pk': valid_id}))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        invalid_id = 100000
        response = self.client.get(reverse('todo-detail', kwargs={'version': 'v1', 'pk': invalid_id}))
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)


class AddTodoTest(BaseViewTest):

    def test_create_a_todo(self):
        response = self.client.post(reverse('todos-list-create',
                                            kwargs={'version': 'v1'}), data=json.dumps({'item': 'fruits'}),
                                    content_type='application/json')

        self.assertEqual(response.data, {'item': 'fruits'})

        # with invalid data
        response = self.client.post(reverse('todos-list-create',
                                            kwargs={'version': 'v1'}), data=json.dumps({'item': ''}),
                                    content_type='application/json')

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)


class UpdateTodoListTest(BaseViewTest):
    def test_update_song(self):
        data = {'item': 'Chocolate'}
        response = self.client.put(
            reverse('todo-detail',
                    kwargs={
                        'version': 'v1',
                        'pk': 2  # update the second todo item
                    }),
            data=json.dumps(data), content_type='application/json'
        )
        self.assertEqual(response.data, data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        invalid_data = {'item': ''}
        response = self.client.put(
            reverse('todo-detail',
                    kwargs={
                        'version': 'v1',
                        'pk': 2  # update the second todo item
                    }),
            data=json.dumps(invalid_data), content_type='application/json'
        )
        self.assertEqual(response.data, invalid_data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class DeleteTodoTest(BaseViewTest):
    pass