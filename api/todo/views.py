from rest_framework import generics, status
from rest_framework.response import Response
from .serializers import TodosSerializer
from .models import Todo
from .decorators import validate_request_data


class ListAndCreateTodosView(generics.ListCreateAPIView):
    """
    Handle both todos/ and POST to /todos
    """
    queryset = Todo.objects.all()
    serializer_class = TodosSerializer

    @validate_request_data
    def post(self, request, *args, **kwargs):
        todo = Todo.objects.create(item=request.data['item'])
        return Response(data=TodosSerializer(todo).data, status=status.HTTP_201_CREATED)


class TodoDetail(generics.RetrieveUpdateDestroyAPIView):
    """
    GET todos/:id/
    PUT todos/:id/
    DELETE todos/:id
    """
    queryset = Todo.objects.all()
    serializer_class = TodosSerializer

    def get(self, request, *args, **kwargs):
        try:
            todo = self.queryset.get(pk=kwargs['pk'])
            return Response(TodosSerializer(todo).data)
        except Todo.DoesNotExist:
            return Response(data={'message': "TODO with id: {} does not exist".format(kwargs['pk'])},
                            status=status.HTTP_404_NOT_FOUND)

    def delete(self, request, *args, **kwargs):
        try:
            self.queryset.get(pk=kwargs['pk']).delete()
            return Response(status=status.HTTP_204_NO_CONTENT)
        except Todo.DoesNotExist:
            return Response(data={'message': "TODO with id: {} does not exist".format(kwargs['pk'])},
                            status=status.HTTP_404_NOT_FOUND)

    def put(self, request, *args, **kwargs):
        try:
            #TODO -> can be done through the serializer as well to DRY out the code a bit
            todo = self.queryset.get(pk=kwargs['pk'])
            todo.item = request.data['item']
            todo.done = request.data['done']

            todo.save()
            return Response(TodosSerializer(todo).data)
        except Todo.DoesNotExist:
            return Response(data={'message': "TODO with id: {} does not exist".format(kwargs['pk'])},
                            status=status.HTTP_404_NOT_FOUND)
