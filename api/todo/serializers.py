from rest_framework import serializers, status
from rest_framework.response import Response
from .models import Todo
from .decorators import validate_request_data


class TodosSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Todo
        fields = ('id', 'item', 'done')

    @validate_request_data
    def post(self, request, *args, **kwargs):
        todo = Todo.objects.create(item=request.data['item'])
        return Response(data=TodosSerializer(todo).data, status=status.HTTP_201_CREATED)
