### Getting started

This is best used with a python virtual environment

### Install Dependencies

`pip install -r requirements.txt`

### Database

The database for this experiment is SQLITE3 (it is inbuilt for most computers with Django installed)

### Migrate the database

`python manage.py migrate`


### To run

`python manage.py runserver`
