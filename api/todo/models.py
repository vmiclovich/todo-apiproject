from django.db import models


class Todo(models.Model):
    item = models.CharField(max_length=255, null=False)
    done = models.BooleanField(default=False)

    def __str__(self):
        return "{}".format(self.item)